from tkinter import *
from fonctions import *
import datetime
import random
import sys


"""initialisations des valeurs"""
lignes = int(sys.argv[1])
colonnes = int(sys.argv[2])
cell_size = int(sys.argv[3])
afforestation = float(sys.argv[4])
temps_ms = int(sys.argv[5])
regle = sys.argv[6]

"""initialisation du canvas"""
master = Tk()
#master.maxsize(colonnes*cell_size,lignes*cell_size) 
#master.minsize(colonnes*cell_size,lignes*cell_size)
canvas = Canvas(master, width=colonnes*cell_size, height=lignes*cell_size)
canvas.pack()
canvas.grid(row=1)

"""initialisations des tableaux de gestion des rectangles"""
rectangle = [[0 for x in range(lignes)] for y in range(colonnes)]
rectangle_color = [[0 for x in range(lignes)] for y in range(colonnes)]

"""initialisation de la foret"""
init_foret(lignes,colonnes,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas)

"""mettre le feu"""
def click_fire(event):
    fonction_clic(lignes,colonnes,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,event,canvas)
canvas.bind('<Button-1>', click_fire)

"""gestion du temps"""
def lol():
    temps(lignes,colonnes,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas)
    master.after(temps_ms,lol)
lol()

def reinit():
    init_foret(lignes,colonnes,cell_size,afforestation,temps_ms,regle,rectangle,rectangle_color,canvas)

Button(master, text ='Réinitialiser le plateau',
        command = reinit).grid(row=2)

Button(master, text="Quitter", bg="orange",
        command = master.quit).grid(row=3)
master.mainloop()
