## About
M3301 /
Mini projet 4 /
Simulations de feux de forêt

## Author
Dupont de vieux pont Alexis /
Ancher Maxime

## Running
python main.py 50 50 10 0.6 500 1
- 1er argument : nb ligne
- 2eme argument : nb colonne
- 3eme argument : taille carrés(ici 10pixels de large)
- 4eme argument : taux de foret(ici 60%)
- 5eme argument : temps(ms) entre chaque tour
- 6eme argument : règle de propagation(1 ou 2)